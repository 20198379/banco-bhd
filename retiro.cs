﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cajero_retiroaut
{
    class Retiro
    {
            public int TipoBHD { get; set; }
            public long Papeleta100 { get; set; }
            public long Papeleta500 { get; set; }
            public long Papeleta200 { get; set; }
            public long Papeleta1000 { get; set; }

            public string RetiroCajero(long monto)
            {
                long Pendiente = monto;

                if (TipoBHD == 1)
                {
                    if (monto % 200 == 0)
                    {
                        Papeleta1000 = Pendiente / 1000;  Pendiente = Pendiente % 1000;

                        Papeleta200 = Pendiente / 200;     Pendiente = Pendiente % 200;
                    }
                    else
                    {
                        return $"Este cajero solo dispensa papeletas de 200 y 1000, debe introducir una cantidad adecuada.";
                    }
                }
                else if (TipoBHD == 2)
                {
                    if (monto % 100 == 0)
                    {
                        Papeleta500 = Pendiente / 500;
                        Pendiente = Pendiente % 500;

                        Papeleta100 = Pendiente / 100;
                        Pendiente = Pendiente % 100;
                    }
                    else
                    {
                        return $"Este cajero solo dispensa papeletas de 100 y 500, debe ingresar un monto adecuado.";
                    }
                }
                else if (TipoBHD == 3)
                {
                    if (monto % 100 == 0)
                    {
                        Papeleta1000 = Pendiente / 1000;
                        Pendiente = Pendiente % 1000;

                        Papeleta500 = Pendiente / 500;
                        Pendiente = Pendiente % 500;

                        Papeleta200 = Pendiente / 200;
                        Pendiente = Pendiente % 200;

                        Papeleta100 = Pendiente / 100;
                        Pendiente = Pendiente % 100;
                    }
                    else
                    {
                        return $"Este cajero solo dispensa papeletas de 100, 200, 500 y 1000, debe ingresar un monto adecuado.";
                    }
                }

                string Resultado = "";
                if (Papeleta1000 > 0)
                {
                    if (Resultado != "")
                    {
                        Resultado = Resultado + ", ";
                    }
                    Resultado = Resultado + $"{Papeleta1000} papeletas de 1000";
                }

                if (Papeleta500 > 0)
                {
                    if (Resultado != "")
                    {
                        Resultado = Resultado + ", ";
                    }
                    Resultado = Resultado + $"{Papeleta500} papeletas de 500";
                }

                if (Papeleta200 > 0)
                {
                    if (Resultado != "")
                    {
                        Resultado = Resultado + ", ";
                    }
                    Resultado = Resultado + $"{Papeleta200} papeletas de 200";
                }

                if (Papeleta100 > 0)
                {
                    if (Resultado != "")
                    {
                        Resultado = Resultado + ", ";
                    }
                    Resultado = Resultado + $"{Papeleta100} papeletas de 100";
                }

                return "Se le entregaron " + Resultado;
                Console.ReadKey();
            }

    }
}



