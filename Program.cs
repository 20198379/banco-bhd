﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cajero_retiroaut
{
   class Program
   {
       static void Main(string[] args)
       {
            int opcion = 0;
            long monto = 0;
            int repetir = 1;

            Console.WriteLine("**********Bienvenido al cajero BHD***********");
            do
            {
                do
                {
                    Console.WriteLine();
                    Console.WriteLine();
                    Console.WriteLine();
                    Console.WriteLine("Seleccione el Tipo de Cajero que desea utilizar:");
                    Console.WriteLine();
                    Console.WriteLine("1- Retirar solo papeletas de 200 y 1000");
                    Console.WriteLine("2- Retiro de papeletas de 100 y 500");
                    Console.WriteLine("3- Retiro eficiente, retira la menor cantidad de papeletas");

                    Console.WriteLine();
                    int.TryParse(Console.ReadLine(), out opcion);

                    if (opcion < 1 || opcion > 3)
                    {
                       Console.Clear();
                       Console.WriteLine("La opcion que introdujo no es válida, reingrese nuevamente\n");
                    }

                } while (opcion < 1 || opcion > 3);

                    Console.WriteLine("Que cantidad desea retirar?");

                 do
                 {
                   long.TryParse(Console.ReadLine(), out monto);
                   if (!(monto > 0))
                   {
                      Console.Clear();
                      Console.WriteLine("la cantidad que introdujo no es correcta, vuelva a ingresarla nuevamente:\n");
                   }
                 } while (!(monto > 0));

                  Retiro retiro = new Retiro();
                  retiro.TipoBHD = opcion;
                  Console.WriteLine(retiro.RetiroCajero(monto));
                  Console.WriteLine("Que desea: \n1) Retirar otra cantidad. \n2) Terminar la transacción.");
                  int.TryParse(Console.ReadLine(), out repetir);

                  Console.Clear();
           } while (repetir == 1);

            Console.ReadKey();
       }

   }
}


                  
                    


                    



